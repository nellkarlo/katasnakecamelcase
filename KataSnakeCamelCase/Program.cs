﻿using System;
using System.Linq;

namespace KataSnakeCamelCase
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(ToCamelCase("is_modal_open_today"));
            Console.WriteLine(ToSnakeCase("getColorCode"));
        }
        public static string ToCamelCase(string text)
        {
            //Helper variables declared
            string input = text;
            string output = "";

            //Get number of occurences of underscores for iteration purpose
            int occurences = text.Count(character => (character == '_'));

            for (int i = 0; i < occurences; i++)
            {
                if (output == "")
                {
                    //If first underscore run this

                    int index = input.IndexOf("_");
                    //Save new lower case to upper case 
                    string newCapital = input[index + 1].ToString().ToUpper();
                    
                    //Do string magic
                    output = input.Remove(index, 2);
                    output = output.Insert(index, newCapital);

                }
                else
                {
                    //If second underscore (changed input to output to keep last iteration)

                    int index = output.IndexOf("_");

                    //Save new lower case to upper 
                    string newCapital = output[index + 1].ToString().ToUpper();

                    //Do string magic
                    output = output.Remove(index, 2);
                    output = output.Insert(index, newCapital);
                }

            }
            return output;
        }
        public static string ToSnakeCase(string text)
        {
            string input = text;
            string output = "";
            
            //Get number of occurences of Capitals in text for iteration purpose
            int occurences = input.ToList().FindAll(character => Char.IsUpper(character)).Count;

            for (int i = 0; i < occurences; i++)
            {
                if (output == "")
                {
                    //get index of first uppercase letter.
                    int index = input.ToList().FindIndex(character => Char.IsUpper(character));

                    //Get character and turn it to lower case
                    string character = Char.ToLower(Convert.ToChar(input[index])).ToString();
                    string underscore = "_";

                    //Insert underscore, replace Capital letter with lower case version.
                    output = input.Insert(index, underscore);
                    output = output.Remove(index + 1, 1);
                    output = output.Insert(index + 1, character);

                }
                else
                {
                    //get index of first uppercase letter.
                    int index = output.ToList().FindIndex(character => (Char.IsUpper(character) && !Char.IsSymbol(character)));
                    
                    //Get character and turn it to lower case
                    string character = Char.ToLower(Convert.ToChar(output[index])).ToString();
                    string underscore = "_";

                    //Insert underscore, replace Capital letter with lower case version.
                    output = output.Insert(index, underscore);
                    output = output.Remove(index + 1, 1);
                    output = output.Insert(index + 1, character);
                }
            }


            return output;
        }
    }
}
